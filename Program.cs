﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Collections.Generic;
using System.IO;

namespace LSB
{
    class Program
    {
        static void Main(String[] args)
        {
            ExampleLSBMethod(@"C:\Users\Admin\Desktop\LSB\img\binaryExample.bmp",
                @"C:\Users\Admin\Desktop\LSB\outExample.bmp",
                @"C:\Users\Admin\Desktop\LSB\Steganography.txt", 
                @"C:\Users\Admin\Desktop\LSB\out.txt");
        }

        private static void ExampleLSBMethod(String nameSrc, String nameDst, String inFile, String outFile)
        {
            Bitmap src = new Bitmap(nameSrc);
            String inputText = File.ReadAllText(inFile);
            List<byte> data = Encoding.UTF8.GetBytes(inputText).ToList();
            //int realcount = data.Count;
            // Выравнивание по модулю 12
            int addCount = 12 - data.Count % 12;
            if (addCount != 0)
            {
                while (addCount != 0)
                {
                    data.Add(0);
                    addCount--;
                }
            }

            // Внедрение информации
            Bitmap dst = LSB.SetLeastSignificantBit(src, data.ToArray());
            dst.Save(nameDst, System.Drawing.Imaging.ImageFormat.Bmp);

            // Извлечение информации
            String resultText = Encoding.UTF8.GetString(LSB.GetLeastSignificantBit(dst, data.Count)); //.Take(realcount).ToArray());
            File.WriteAllText(outFile, resultText, Encoding.UTF8);

            String folder = String.Join("\\", outFile.Split('\\').Reverse().Skip(1).Reverse());
            // Просмотр последних битов
            LSB.ViewBit(dst, 3).Save(folder + "\\dstView.bmp", System.Drawing.Imaging.ImageFormat.Bmp);
            LSB.ViewBit(src, 3).Save(folder + "\\srcView.bmp", System.Drawing.Imaging.ImageFormat.Bmp);

            src.Dispose();
            dst.Dispose();
        }
        private static void ExampleAdditivMethod(String nameSrc, String nameDst, String inFile, String outFile)
        {
            Bitmap src = new Bitmap(nameSrc);
            String inputText = File.ReadAllText(inFile);
            List<byte> data = Encoding.UTF8.GetBytes(inputText).ToList();
            
            // Внедрение информации
            Bitmap dst = Additive.EmbedInformation1(src, data.ToArray());
            dst.Save(nameDst);

            // Извлечение информации
            String resultText = Encoding.UTF8.GetString(Additive.ExtractInformation1(src, dst, data.Count));
            File.WriteAllText(outFile, resultText, Encoding.UTF8);

            src.Dispose();
            dst.Dispose();
        }

        private void ExampleAdditiveMethod2(String nameSrc, String nameDst, String inFile, String outFile)
        {
            Bitmap src = new Bitmap(nameSrc);
            String inputText = File.ReadAllText(inFile);
            List<byte> data = Encoding.UTF8.GetBytes(inputText).ToList();
            
            int addCount = 9 - data.Count % 9;
            if (addCount != 0)
            {
                while (addCount != 0)
                {
                    data.Add(0);
                    addCount--;
                }
            }

            // Внедрение информации
            Bitmap dst = Additive.EmbedInformation(src, data.ToArray());
            dst.Save(nameDst);

            // Извлечение информации
            String resultText = Encoding.UTF8.GetString(Additive.ExtractInformation(src, dst, data.Count));
            File.WriteAllText(outFile, resultText, Encoding.UTF8);

            src.Dispose();
            dst.Dispose();
        }
    }
}
