﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
namespace LSB
{
    // https://msdn.microsoft.com/ru-ru/library/system.drawing.color%28v=vs.110%29.aspx
    // https://msdn.microsoft.com/ru-ru/library/system.drawing.bitmap%28v=vs.110%29.aspx
    public class LSB
    {
        public static Bitmap SetLeastSignificantBit(Bitmap srcImg, byte[] data)
        {
            Color color1, color2, color3, color4, color;
            Bitmap dstImg = new Bitmap(srcImg.Width, srcImg.Height);
            
            int i = 0;
            for(int y = 0; y < srcImg.Height; ++y)
            {
                for(int x = 0; x < srcImg.Width - 3; x+=4)
                {
                    if (i < data.Length - 2)
                    {
                        byte d1 = data[i];
                        byte d2 = data[i + 1];
                        byte d3 = data[i + 2];

                        i += 3;

                        color1 = srcImg.GetPixel(x, y);
                        color2 = srcImg.GetPixel(x + 1, y);
                        color3 = srcImg.GetPixel(x + 2, y);
                        color4 = srcImg.GetPixel(x + 3, y);

                        color = Color.FromArgb((byte)(color1.R & 252 | ((d1 & 255) >> 6)),
                                               (byte)(color1.G & 252 | ((d1 & 63)  >> 4)),
                                               (byte)(color1.B & 252 | ((d1 & 15)  >> 2)));
                        dstImg.SetPixel(x, y, color);

                        dstImg.SetPixel(x + 1, y, Color.FromArgb((byte)(color2.R & 252 | ((d1 & 3)   >> 0)),
                                                                 (byte)(color2.G & 252 | ((d2 & 255) >> 6)),
                                                                 (byte)(color2.B & 252 | ((d2 & 63)  >> 4))));

                        dstImg.SetPixel(x + 2, y, Color.FromArgb((byte)(color3.R & 252 | ((d2 & 15)  >> 2)),
                                                                 (byte)(color3.G & 252 | ((d2 & 3)   >> 0)),
                                                                 (byte)(color3.B & 252 | ((d3 & 255) >> 6))));

                        dstImg.SetPixel(x + 3, y, Color.FromArgb((byte)(color4.R & 252 | ((d3 & 63) >> 4)),
                                                                 (byte)(color4.G & 252 | ((d3 & 15) >> 2)),
                                                                 (byte)(color4.B & 252 | ((d3 & 3)  >> 0))));
                    }
                    else
                    {
                        dstImg.SetPixel(x, y, srcImg.GetPixel(x, y));
                        dstImg.SetPixel(x + 1, y, srcImg.GetPixel(x + 1, y));
                        dstImg.SetPixel(x + 2, y, srcImg.GetPixel(x + 2, y));
                        dstImg.SetPixel(x + 3, y, srcImg.GetPixel(x + 3, y));
                    }
                }
            }

            return dstImg;
        }

        public static byte[] GetLeastSignificantBit(Bitmap dstImg, int count)
        {
            byte[] bytes = new byte[count];

            Color color1, color2, color3, color4;

            int i = 0;
            for (int y = 0; y < dstImg.Height; ++y)
            {
                for (int x = 0; x < dstImg.Width - 3; x += 4)
                {

                    color1 = dstImg.GetPixel(x, y);
                    color2 = dstImg.GetPixel(x + 1, y);
                    color3 = dstImg.GetPixel(x + 2, y);
                    color4 = dstImg.GetPixel(x + 3, y);

                    bytes[i]     = (byte)(((color1.R & 3) << 6) | ((color1.G & 3) << 4) | ((color1.B & 3) << 2) | ((color2.R & 3) << 0));
                    bytes[i + 1] = (byte)(((color2.G & 3) << 6) | ((color2.B & 3) << 4) | ((color3.R & 3) << 2) | ((color3.G & 3) << 0));
                    bytes[i + 2] = (byte)(((color3.B & 3) << 6) | ((color4.R & 3) << 4) | ((color4.G & 3) << 2) | ((color4.B & 3) << 0));
                    i += 3;

                    if (i >= count) return bytes;
                }
            }

            return bytes;
        }

        public static Bitmap ViewBit(Bitmap srcImg, int mask)
        {
            Bitmap dstImg = new Bitmap(srcImg.Width, srcImg.Height);
            Color color;

            for (int y = 0; y < srcImg.Height; ++y)
            {
                for (int x = 0; x < srcImg.Width; ++x)
                {
                    color = srcImg.GetPixel(x, y);
                    dstImg.SetPixel(x, y, Color.FromArgb((color.R & mask) == mask ? (byte)255 : (byte)0,
                                                         (color.G & mask) == mask ? (byte)255 : (byte)0,
                                                         (color.B & mask) == mask ? (byte)255 : (byte)0));
                }
            }

            return dstImg;
        }
    }
}
