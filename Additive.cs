﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSB
{
    public class Additive
    {
        private static int L = -15;
        public static Bitmap EmbedInformation(Bitmap srcImg, byte[] data)
        {
            Color c;
            Bitmap dstImg = new Bitmap(srcImg.Width, srcImg.Height);

            int k = 0;

            int r, g, b, color;
            for (int y = 0; y < srcImg.Height - 4; y += 5)
            {
                for (int x = 0; x < srcImg.Width - 4; x += 5)
                {
                    if (k < data.Length - 8)
                    {

                        for (int j = 0; j < 5; ++j)
                        {
                            for (int i = 0; i < 5; ++i)
                            {
                                c = srcImg.GetPixel(x + i, y + j);

                                if ((j & 1) == 0 && (i & 1) == 0) // j и i - четные
                                {
                                    //Color color = Color.FromArgb(c.ToArgb() * (1 + L * data[k]));
                                    //Color color = Color.FromArgb(c.ToArgb() + L * data[k]);
                                    color = 0;
                                    r = c.R;
                                    g = c.G;
                                    b = c.B;

                                    color |= b;
                                    color |= g << 8;
                                    color |= r << 16;

                                    color = color + L * data[k];

                                    r = (color >> 16) & 255;
                                    g = (color >> 8) & 255;
                                    b = color & 255;

                                    dstImg.SetPixel(x + i, y + j, Color.FromArgb(r, g, b));

                                    k++;

                                }
                                else
                                {
                                    dstImg.SetPixel(x + i, y + j, c);
                                }
                            }
                        }

                    }
                    else
                    {
                        for (int j = 0; j < 5; ++j)
                        {
                            for (int i = 0; i < 5; ++i)
                            {
                                dstImg.SetPixel(x + i, y + j, srcImg.GetPixel(x + i, y + j));
                            }
                        }
                    }
                }
            }

            return dstImg;
        }

        public static byte[] ExtractInformation(Bitmap srcImg, Bitmap dstImg, int count)
        {
            byte[] bytes = new byte[count];

            Color cSrc, cDst;

            int k = 0;
            for (int y = 0; y < dstImg.Height - 4; y += 5)
            {
                for (int x = 0; x < dstImg.Width - 4; x += 5)
                {

                    for (int j = 0; j < 5; ++j)
                    {
                        for (int i = 0; i < 5; ++i)
                        {
                            if ((j & 1) == 0 && (i & 1) == 0) // j и i - четные
                            {
                                cSrc = srcImg.GetPixel(x + i, y + j);
                                cDst = dstImg.GetPixel(x + i, y + j);
                                int idst = cDst.ToArgb() & 16777215;
                                int isrc = cSrc.ToArgb() & 16777215;
                                bytes[k] = (byte)((idst - isrc) / L);
                                k++;
                                if (k >= count) return bytes;
                            }
                        }
                    }

                }
            }

            return bytes;
        }

        // ---

        private static int step = 100; // 100 50
        private static int countInStep = 5;

        public static Bitmap EmbedInformation1(Bitmap srcImg, byte[] data)
        {
            Color c;
            Bitmap dstImg = new Bitmap(srcImg);

            int k = 0;

            for (int y = 0; y < dstImg.Height; y+= step / countInStep) // - step + 1
            {
                for (int x = 0; x < dstImg.Width; x+= step / countInStep)
                {
                    c = srcImg.GetPixel(x, y);

                    Color color = Color.FromArgb(c.ToArgb() + L * data[k]);
                               
                    dstImg.SetPixel(x, y, color);

                    k++;
                    if (k >= data.Length) return dstImg;
                }
            }

            return dstImg;
        }

        public static byte[] ExtractInformation1(Bitmap srcImg, Bitmap dstImg, int count)
        {
            byte[] bytes = new byte[count];

            Color cSrc, cDst;

            int k = 0;
            for (int y = 0; y < dstImg.Height; y += step / countInStep) // - step + 1
            {
                for (int x = 0; x < dstImg.Width; x += step / countInStep)
                {

                    cSrc = srcImg.GetPixel(x, y);
                    cDst = dstImg.GetPixel(x, y);
                    int idst = cDst.ToArgb();
                    int isrc = cSrc.ToArgb();
                    bytes[k] = (byte)((idst - isrc) / L);
                    k++;
                    if (k >= count) return bytes;
                }
            }

            return bytes;
        }

    }
}
    
